public class Paciente {
	double peso;
	double altura;

	public Paciente(double peso, double altura) {
		this.peso = peso;
		this.altura = altura;
	}

	public double calcularIMC() {
		double imc = 0;

		imc = peso / (altura * altura);
		return imc;
	}

	public String diagnostico() {
		if (calcularIMC() < 16)
			return "Baixo peso muito grave";
		if (calcularIMC() < 17)
			return "Baixo peso grave";
		if (calcularIMC() < 18.50)
			return "Baixo peso";
		if (calcularIMC() < 25)
			return "Peso normal";
		if (calcularIMC() < 30)
			return "Sobrepeso";
		if (calcularIMC() < 35)
			return "Obesidade grau I";
		if (calcularIMC() < 40)
			return "Obesidade grau II";
		else
			return "Obesidade grau III";
	}
}
